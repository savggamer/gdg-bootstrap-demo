import { GdgDemoPage } from './app.po';

describe('gdg-demo App', () => {
  let page: GdgDemoPage;

  beforeEach(() => {
    page = new GdgDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
