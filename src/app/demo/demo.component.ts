import { Component, OnInit } from '@angular/core';
import {NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModalComponent} from "../modal/modal.component";

@Component({
  selector: 'kalieki-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  date : NgbDateStruct;

  constructor(private modalService : NgbModal) { }

  ngOnInit() {
  }

  sayHello(){
    let modal = this.modalService.open(ModalComponent);
    modal.componentInstance.name = 'Seth';
  }

}
