import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from "ng2-toastr";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  ngOnInit(): void {
    this.toasts.success("Yay", "App initialized successfully")
  }


  constructor(private toasts : ToastsManager, private ref : ViewContainerRef) {
    toasts.setRootViewContainerRef(ref);
  }

  title = 'app works!';

}
