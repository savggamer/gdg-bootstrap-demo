import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'kalieki-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  inputs:['name']
})
export class ModalComponent implements OnInit {

  name : string;

  constructor(private activeModal : NgbActiveModal) { }

  ngOnInit() {
  }

}
