import {ToastOptions} from "ng2-toastr";
export class KaliekiOptions extends ToastOptions{
  positionClass: 'toast-bottom-center';
  animate = 'flyRight'; // you can override any options available
  newestOnTop = false;
  showCloseButton = true;
}
