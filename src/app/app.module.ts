import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ToastModule, ToastOptions} from "ng2-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { HeaderComponent } from './header/header.component';
import {DemoComponent} from "./demo/demo.component";
import { ModalComponent } from './modal/modal.component';
import {KaliekiOptions} from "./KaliekiOptions";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DemoComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    ToastModule.forRoot()
  ],
  providers: [
    {provide: ToastOptions, useClass: KaliekiOptions},
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent]
})
export class AppModule { }
